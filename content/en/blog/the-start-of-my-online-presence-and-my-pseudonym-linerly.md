---
title: The start of my online presence and my pseudonym, “Linerly”
date: 2024-07-31T17:36:00+07:00
draft: false
---
*If you've seen [my Games page](/en/games) and visited [my Scratch profile](https://scratch.mit.edu/users/DancingLine), you might be wondering about my username on Scratch, `DancingLine`.*

# Starting on Scratch

Since I was in elementary school—around 2018—I was on Scratch trying to create fan-made levels of the mobile game *Dancing Line*. Of course, that led me to use `DancingLine` as my Scratch username. I ended making more than (glitchy) fan-made levels from a template that someone had made on Scratch a long time ago, and I ended up unsharing those Scratch projects because they were broken.

I began to use Scratch as a place for me to share my creativity, but they were still unshared… until I tried asking my mother whether I could share my projects online. Furthermore, I tried “asking” because I wasn't sure if I really wanted to share them—I was pretty cautious about using the internet back then.

As I made projects on Scratch, lots of people started to know me a little. It felt so nice being able to leave a comment and get a reply, even if it takes a while to respond back. We never shared any outside contacts, so we just expect that everyone stays there every day. Eventually, I made my first online friends on Scratch—we still get in touch with each other today, though not as often since I got into high school.   

# …then on Discord

When I searched about the mobile game Dancing Line, I discovered that there was a community on Discord related to it—*Dancing Line Discord*. I joined the server right away, but that also means I joined Discord too (and yes, I was underaged, but it didn't really matter). I still remembered sending a few “…” when I first joined, and eventually I get to know people there.

As for the server itself, it got deleted after a few years, a new server was created (named *Rhythm Games Hub*) … and then got deleted again in a year or so.

For as long as I was on Discord, I managed to get some of my friends from Scratch to Discord using my server on a billboard that I made on Scratch. Though I got a warning not to share outside contacts.

This is where my pseudonym slightly changed, from `DancingLine` to `Liner`, but the “-ly” suffix comes next.   

# …then finally, on GitHub

When I decided to make a Discord bot, Heroku (the platform I used to host it for free, but not anymore) needs the source code to be on GitHub. Eventually I registered for [a GitHub account](https://github.com/Linerly) (though I use [Codeberg for all my personal projects](https://codeberg.org/Linerly) now).

The only problem was that the username `Liner` had been taken… I didn't like any of the username suggestions, so I added the suffix “-ly”. It's available, and I used that username across all accounts that I have.

…and thus the pseudonym `Linerly` that I've been using for years now, even before [I registered `linerly.xyz`](../first-anniversary-of-linerly-xyz).
