---
title: "A new domain name that I finally own!"
date: 2023-08-01T20:34:06+07:00
---

My website is now on linerly.xyz (previously on linerly.codeberg.page, linerly.tk, and linerly.github.io). Unlike the old linerly.tk domain (which a friend got one for free to me from Freenom), this one is mine. Of course, I paid for it (for cheap).

Unfortunately with domains that you can get for free, it's not really reliable whether you will be able to use it for a long time. Fortunately, I didn't use it for more permanent things (such as email). With the new domain, I should be able to use it for a lot more things now!

It is a bit of pain trying to change everything from the Pages settings, all the way to my profiles on various websites. But, this is worth it in the end---it won't change for a long time or so.
