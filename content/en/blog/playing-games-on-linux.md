---
title: "Playing games on Linux"
date: 2021-03-28
---

It's probably been a while since I've moved from using Windows 10 to Arch Linux now, so far Arch Linux already provides the things I need.

One thing that baffles me for quite a while is [gaming on Linux](https://itsfoss.com/linux-gaming-guide/). This isn't really a big issue at all for me because I rarely play Windows games, I usually play native Linux games and some browser games instead.

If I wanted to play a Windows game like Roblox it will not work using [Wine](https://www.winehq.org/). There are several tools out there for playing Roblox on Linux like [Grapejuice](https://gitlab.com/brinkervii/grapejuice) or [Roblox Linux Wrapper](https://github.com/roblox-linux-wrapper/roblox-linux-wrapper), however I couldn't get one of them to work. Let's just see if Roblox will make a Linux build in the future, but for now I could play Roblox on my phone.

When you can't play a Windows game in Linux using Wine, your options would be to boot into Windows and play it there or to play it in a Windows virtual machine as a last resort.

I've also heard of other compatibility layers such as [Anbox](https://anbox.io/) and [Darling](https://darlinghq.org/) as well, however I haven't tried Darling in Arch Linux (I'm lazy to build it, takes a large amount of space, takes time to build it, and I don't really know anything about macOS since I don't use macOS). Anbox works fine when I used Ubuntu, and I was curious if Android games will run using Anbox, so I did, however I couldn't get them to work.

I'll probably make another article about this again in the future once gaming on Linux becomes better...
