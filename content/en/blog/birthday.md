---
title: "Birthday"
date: 2021-05-04
---

It's my birthday! :)

I don't want much for my birthday, but for those of you who are looking at this short post, please stay safe and healthy.
Have a great week. :D
