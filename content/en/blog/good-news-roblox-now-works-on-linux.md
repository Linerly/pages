---
title: Good news, Roblox now works on Linux!
date: 2021-06-08
---

## 2025 UPDATE

You can now play Roblox on Linux with [Sober](https://sober.vinegarhq.org)—it uses the Android version of Roblox and has its own compatibility layer to run Roblox on Linux, much like how the [Minecraft Bedrock Launcher ("Unofficial *NIX Lancher for Minecraft")](https://minecraft-linux.github.io) works.

Unfortunately, the Sober project is currently closed-source for *obvious* reasons. We definitely don't want to get {{< abbr "RoL" "Roblox on Linux" >}} broken again should Roblox blocks the client.

---

### *Old versions of this article are retained for history.*

## ~~2024 UPDATE~~
~~This didn't last long. There has been Wine-based cheats and exploits for quite a while, thus causing them to stop unofficial support for Roblox to run on Linux via Wine. We could say farewell to Roblox on Linux... for now.~~


## ~~2023 UPDATE~~
~~Roblox has implemented Byfron's anti-cheat system (which is Hyperion) and the client is now 64-bit instead of 32-bit.~~

~~Although they have blocked Roblox from running on Linux using Wine for a while, they've kept the [promise to make it work again (unofficially)](https://devforum.roblox.com/t/why-did-roblox-stop-supporting-linux-users/2444335/61)! At the time of editing this post, you will need to wait until it's [officially announced](https://devforum.roblox.com/t/why-did-roblox-stop-supporting-linux-users/2444335/100). You can try setting the channel to `zintegration` if you're impatient, though this might cause some problems.~~

~~P.S. You'll need to log in to the Roblox DevForum.~~

---

~~Finally, thanks to Wine 6.11, Roblox now works on Linux! No need to use a virtual machine, although there are some bugs that can affect your gameplay. If you're using Wine 7.10 or later to play Roblox, it should be running flawlessly — no need to compile Wine with the right-click bug patch.~~

~~I've been playing Roblox on Manjaro (and Arch Linux) for a while, and it is pretty much just the same experience as playing it on Windows. Roblox Studio _should_ work as well, though I couldn't log in to Roblox Studio — Grapejuice has a way to log in to Roblox Studio from the Grapejuice application.~~

~~To make setting up Roblox on Linux easy, I used [Grapejuice](https://brinkervii.gitlab.io/grapejuice/) — and you can [find the instructions here](https://brinkervii.gitlab.io/grapejuice/docs/) to set up Roblox using Grapejuice.~~
