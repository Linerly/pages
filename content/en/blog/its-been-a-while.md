---
title: It’s been a while…
date: 2024-02-29T15:27:26.970Z
draft: false
---

It's a new year, a new month… _a leap year?_

Anyway, I haven't posted anything to my blog in such a long time. Although I [very recently added Decap CMS to my site](https://codeberg.org/Linerly/pages/commit/faa874275e910184267a9509decb7864a00c74df) to make posting much easier from anywhere, I'm still not sure what I want to post. Even so in Indonesian, though I don't have to translate _every_ post if I don't want to.

In the meantime, I still have a lot of commitments outside my blog anyway…

However, now that my blog (and my website as a whole) has a CMS interface, I can now make blog posts without having to open a whole text editor, a terminal, and whatever else.

~~…spoiler alert, I haven't figured out how to set up media properly in the CMS, so I'll have to fiddle again in a bit.~~

**edit (2024-03-02)**: Apparently you need to have _all_ the images be in one folder to make the Media view in Decap CMS work, so you can't really organize assets in folders.
