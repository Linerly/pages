---
title: First anniversary of linerly.xyz
date: 2024-07-31T16:58:07+07:00
draft: false
---

It's been a year now since I first registered `linerly.xyz`!

I've been using my own domain not just for my own stuff, but also for some contacts (custom email address with [Proton Mail](https://proton.me/mail)) and some self-hosted services as well ([`owntracks-recorder`](https://github.com/owntracks/recorder), [`owntracks-frontend`](https://github.com/owntracks/frontend), and [`immich`](https://github.com/immich-app/immich)).

…and that's all I have to say.

*Good thing I didn't forget about the renewal—here's for the next year!*
