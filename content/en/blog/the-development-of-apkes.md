---
title: "The development of Apkes"
date: 2022-08-25T23:47:42+07:00
---

_[This post is also available in Indonesian.](/id/blog/pengembangan-apkes)_

# Have you used web apps?

You may have used web apps on your phone or laptop without knowing about it, but web apps are basically just websites designed to be like an app.

# What is Apkes?

Apkes is a blend of “aplikasi” and “kesehatan”, which means “app” and “health” respectively in Indonesian. The app is built with [the Svelte framework](https://svelte.dev) and other things such as [Tailwind](https://tailwindcss.com) to make development as fast as possible in two weeks or so.

![Apkes screenshot](/media/Screenshot_20220825_235138.png "A screenshot of Apkes. Yes, I had to type this post at night due to me going places.")

Indonesian was the only language that is supported, though it now supports English thanks to internationalization in Svelte. Other languages can be added as well through a pull request on [the Codeberg repository](https://codeberg.org/Linerly/apkes/pulls).

The inspiration for the web app actually came from meditation apps and such.

I intentionally made it simple so that it's lightweight as possible and easy to maintain if I decide to add new things (hopefully).

As with my other projects like this website, Apkes is open-source — anyone can view the source code to see how it works, [see the source code on Codeberg](https://codeberg.org/Linerly/apkes).

# Steps of making it

There are many things that are used behind the scenes — all of which are open-source — such as

- The libraries and frameworks: [Svelte](https://svelte.dev), [Tailwind](https://tailwindcss.com), [Vite](https://vitejs.dev), for the framework, styling, and building respectively.
- Platforms for hosting: [Codeberg](https://codeberg.org), [Codeberg Pages](https://codeberg.page), [Codeberg CI](https://codeberg.org/Codeberg-CI/request-access#getting-access-to-the-codeberg-ci), [deSEC](https://desec.io), for source code hosting, web app hosting, app building, and domain name servers respectively.
- The tools: [Git](https://git-scm.com) and [VS Codium](https://vscodium.com), for the version control system and code editor, respectively.

Furthermore, I also tested the app with my laptop and my phone with Chromium and Firefox. I don't have the chance to test it on Apple devices — or WebKit-based browsers — but it should work.

# How it _actually_ works

The features are separated as Svelte components in order not to clutter things.

Unlike other frameworks, Svelte compiles the app so that it can run quickly in the browser.

Currently, the translations are not embedded in the app, but rather getting the appropriate translation on-demand according to the browser language settings.

After all of that, the app will work without an internet connection — until you refresh the page — as all data is stored locally in the browser.

# Features in Apkes

Currently, there are just 3 features (also known as “widgets”) — Suggestions, Quotes, and Reflection.

- **Suggestions** — well, suggestions, about living a healthy life.
- **Quotes** — read quotes about _why_ you should be living a healthy life.
- **Reflection** — type whatever that comes to your mind, such as your mood, feeling, or things that have happened. It also saves automatically, so you can see your reflections in the app as long you don't delete your browser data.

# Issues while developing

Of course, like any software development, issues can happen at any time. Here's the thing: _be patient, calm down, and try to find the solution_. Still can't find the solution? Leave it for a while, then come back whenever you're _ready_.

# The results…

You can see it for yourself — give the web app a try by going to https://apkes.linerly.tk

Feedbacks, ideas, and other things would be appreciated either in [the Codeberg repository](https://codeberg.org/Linerly/apkes/issues) (in the [Issues](https://codeberg.org/Linerly/apkes/issues) section) or [sending it directly to me](/en/social-media). :)
