---
title: "Privacy-friendly (meta)search engines"
date: 2021-04-24
---

I've been recently gotten into these privacy-friendly metasearch engines like searx and Whoogle.

~~...and by the way, I hosted searx on Replit. If you want to try using it, [feel free to do so](https://searx.linerly.repl.co).~~ (update: I retired it a long time ago, so it won't work anymore)

There are also other searx and Whoogle instances that you can use publicly, however if you don't trust the instance's maintainer (including me as well), you can also host it by yourself.

If you don't want to host your own searx or Whoogle instance, there are also privacy-friendly search engines as well like [DuckDuckGo](https://duckduckgo.com/), [Startpage](https://startpage.com/) (which I also like), [Qwant](https://www.qwant.com/), [Swisscows](https://swisscows.com/), and possibly even more.

Using either of these (meta)search engines, you can easily avoid being tracked by Google. (not really, most websites that you're visiting from the [meta]search engines might track you as well)

_Edited on 2021-10-23: I don't use Whoogle anymore, so I removed it from Replit and in this article as well._
