---
title: "What's new?"
date: 2022-09-23T08:25:28+07:00
---

There are many things that I did for my website, such as:

- Using Hugo to make everything in one place, instead of being separated across different repositories (at least on GitHub, but now everything has been migrated to Codeberg).
- Redesigning the website.
- Making it available in Indonesian, my native language.

…and probably more in the future, see the [notes](/en/notes)

With this new website, hopefully I should be able to make more blog posts in the future… when I have the time.
