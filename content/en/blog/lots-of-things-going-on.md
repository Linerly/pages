---
title: "Lots of things going on..."
date: 2021-10-23
---

Several improvements on my website, including but not limited to:

- Gradient instead of tiled images for the background.
- Replacing Uptime Robot with [Upptime](https://github.com/Linerly/status).
- Adding [a gallery of my creations that I made](/creations) (finally)!
- ~~Updated the LinerlyBot page with the addition of [LinerlyBot Matrix](https://matrix.to/#/@linerlybot-matrix:matrix.org)~~ (though not yet in the new site).
- …and more!

Speaking of Matrix, I've moved most of my conversations to Matrix (just to keep them in one place :D), either by asking some of my friends and family (which I installed a Matrix client on their devices) to use Matrix or by using a bridge like [a public Discord bridge hosted by t2bot](https://t2bot.io/discord) to bridge some Discord servers and [a public WhatsApp bridge hosted by tchncs](https://tchncs.de/matrix).

After a while setting up the Discord bridge in MediaPlay Discord, there were a few things about the bridge, like why are Matrix users appearing as a bot, what is Matrix, why not use Discord, why use Matrix, …

Sending a message in a Matrix room to a Discord server might reduce privacy, but it really reduces the amount of data that Discord can collect — just the message content, avatar, and username. Apparently Discord bots like Dyno and Auttaja can't log messages sent from a webhook because of the Discord API limitations. I'm also aware about those message logging plugins for BetterDiscord — it's the internet, once you put something in there, it won't be gone immediately.

Of course, my school uses WhatsApp, since it is the most common messaging app. Trying to move an entire class or even a community to yet another messaging app isn't that easy. What if WhatsApp disappears? At the very least, WhatsApp appears to be end-to-end encrypted.

As for Telegram, it doesn't have end-to-end encryption enabled by default, and it's only available for the Secret Chats feature.

I've also been contributing translations to several Matrix clients, including [Element (Android)](https://element.io/get-started#download), [SchildiChat (Android)](https://schildi.chat/android), [FluffyChat](https://fluffychat.im), and [nheko](https://nheko-reborn.github.io).

Speaking of self-hosting, I also host things like [SearXNG](https://github.com/SearXNG/SearXNG) on [Replit](https://replit.com), and a few bots on Replit as well. I used to self-host a few things at home, but I no longer do that.

Anyway, that's all I have in this article. This article might be the longest one in my blog.
