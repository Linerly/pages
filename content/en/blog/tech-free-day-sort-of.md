---
title: "Tech-free day (sort of)"
date: 2023-09-09T20:15:33+07:00
---

How much time do you spend online? Probably too much. Do you try to keep up with a lot of things on your phone or any other device? Most of us do.

I didn't realize it, but I guess I can be tired from using tech. That is, until my class has decided to go on a "One Day No Phones" challenge. (Although technically it restricts using phones for a day, I also take the time to store my laptop away.)

Coincidentally, I also went on a survey to various places, so that fills up the time I disconnected. The rest were naps before I go on the survey.

The challenge was supposed to apply to my parents as well, but I guess they need to use the phone for important matters. I had to use my phone for a bit to reconfigure NextDNS for all devices in the family and disabling parental controls (I'm the only one who controls the DNS, not them).

After the challenge ends, I also took the time to make my phone slightly minimal — that is, deleting unused apps and only placing most-used apps on the home screen in one screen.

I also tried using auto dark mode, so that in daytime it uses the light theme, and the dark theme at nighttime. It quickly turned into an eyesore though, so I keep using dark mode all day. My devices' body is black anyway.

On my Android phone, I also used grayscale mode at bedtime. It does help with reducing all distractions, but it's not very useful if you need to view content that relies on color.
