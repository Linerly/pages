---
title: "Yet another (minor) redesign"
date: 2025-01-04T14:30:42+07:00
---

A new year, a new design… because, *why not?*

Being older myself means keeping up with my personal branding, somehow.

# Talking about fonts
When I was making my website back in 2019, I had used [Quicksand](https://github.com/andrew-paglinawan/QuicksandFamily) as the font, [right in this commit](https://codeberg.org/Linerly/pages/commit/9dcab964e6443f2fba7531e01605a40551e15cb3) when I was just learning on how to make a website from scratch by myself without relying on Google Sites, which I used to build my very first website; it's long gone by now...

That's around 5 years ago. Even then, I was still using various different fonts (most of which weren't openly licensed) for many of my profiles back then.

Quicksand looks nice, it just doesn't serve me anymore now[^1]. It felt too "playful" when it comes to being professional. I know this is silly, but I've never considered making my site "professional" anyway because this website has always been a "silly" one.

If I want to build a *personal branding*, might as well do it now. The font is just one part of it.

When I was considering a new font, I came across [Inclusive Sans](https://github.com/LivKing/Inclusive-Sans) (plus [Iosevka](https://github.com/be5invis/Iosevka) for `monospaced texts`[^2]) which are *perfect* for my redesign---they seem pretty legible. Especially for Inclusive Sans, _it has dedicated **italic** versions!_

# Layout changes

I haven't significantly changed much of the existing website layout, other than the fonts. The only changes so far are the spacing and sizes of fonts, the width of the content, etc. The previous iteration of the layout made blog posts feel *tight* and *uncomfortable* to read (at least, for *me*).

[The Notes page](/en/notes/#headings) has an example of how the layout will work, just in case if I make a post that has those elements. I've also fixed some of the smaller problems on other pages.

The language selector on top of the page now displays the full language names, not just the abbreviations.

![The website navigation bar with the new language selector.](/media/Screenshot%202025-01-04%20at%2014-03-42%20Yet%20another%20(minor)%20redesign%20-%20Linerly.png "Doesn’t it look nicer?")

# *More changes?*

I don't know, but this is still new. I'm currently trying to figure out what's right for me...

[^1]: I still have a soft spot for Quicksand, like using it for sticking up quotes in my room.
[^2]: This actually pairs well with Inclusive Sans in my opinion, though I had to adjust the `font-features-settings` for Iosevka so that the letter "a" is similar with both fonts being used.