---
title: "New blog implementation"
date: 2021-03-28
---

Hello everyone, I really wanted to make a blog a really long time ago, but I just didn't know on how to set it up. But now I do!

My existing pages in my website will be still there in my [linerly.github.io repository.](https://github.com/Linerly/linerly.github.io) In this case, my blog is in the [blog repository.](https://github.com/Linerly/blog)

I will probably use this for my updates, tutorials, tricks and tips, and more. I guess I'll have to set up my blog further…
