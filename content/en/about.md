---
title: "About me"
date: 2022-08-21T19:58:15+07:00
layout: staticpage
---

**Hello there!** 👋

I'm a web developer, designer, and an artist. I do other things in my spare time, but those aren't really my main thing.

---

# Skills
## Web Development
- HTML, CSS, JavaScript[^1]
- Svelte (and SvelteKit)
- Hugo

## Software Development
- Python[^2]

## Game Development
- Godot Engine
- Scratch

## Design
- UI/UX design using Inkscape and Penpot[^3]
- Occasionally drawing using Krita

## Languages
- Indonesian (native)
- English (C1)
- French (A1)[^4]

# Certifications
## Languages
- {{< abbr "UKBI" "Uji Kemahiran Berbahasa Indonesia" >}} — {{< abbr "696" "Sangat Unggul" >}} (2024-11-20)
- {{< abbr "TOEFL ITP" "Test of English as a Foreign Language - Institutional Testing Program" >}} — {{< abbr "627" "C1" >}} (2025-02-11)

## Networking
- {{< abbr "MTCNA" "MikroTik Certified Network Associate" >}} (2025-02-20)[^5]

# Strengths
- Whenever I have problems, I could solve them… most of the time. But sometimes it can take a while. Sometimes resorting to a different idea when something doesn't work many times.
- I enjoy creating projects from scratch, be it a web app, a game, or even a simple bot.
- Everyday learning is a must. That's how I get into these.
- I’m used to working on long-term projects, even when they get challenging, like maintaining my home server or debugging an app… hopefully.

# Motivations
- Create things that are useful, or fun at the very least.
- Projects aren't always a destination—it's a journey!
- Being open means improving together over time; that's the reason my projects are open-source.

[^1]: I mostly use TypeScript when it comes to making Svelte web apps, but I have no idea how to use TypeScript properly… mostly with the *types* themselves.
[^2]: For now…
[^3]: WIP: Sharing UI concepts soon.
[^4]: Still learning!
[^5]: I got 100% on the exam. Yes, it was open-book.