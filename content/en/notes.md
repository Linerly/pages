---
title: Notes
date: 2022-08-20T16:22:14+07:00
layout: staticpage
---

This website now uses [Hugo](https://gohugo.io) to make editing things easy, especially on blog posts. :)

[The source code has also migrated to Codeberg.](https://codeberg.org/Linerly/pages)

The new site is also hosted on [Codeberg Pages](https://codeberg.page), and [Codeberg CI](https://codeberg.org/Codeberg-CI/request-access#getting-access-to-the-codeberg-ci) (though the CI is in a closed testing phase for now).

The site also used to have Google Analytics a long time ago, however it has been removed ever since due to privacy concerns of it — I did get some useful data about how many visits my site has.

Because of that, the site now uses [GoatCounter](https://www.goatcounter.com) for analytics — [see the public analytics page for my site](https://stats.linerly.xyz)!

---

Also, there are more formatting styles! Here are some examples:

## Headings

# Heading 1

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

## Horizontal Rule

---

## Emphasis

**Bold text**, _italic text_, **_bold & italic text_**

## Blockquote

> This is a blockquote.

## Lists

### Unordered

- Item 1
- Item 2
  - Sub-item 2.1
  - Sub-item 2.2

### Ordered

1. First item
2. Second item
3. Third item

## Code

Inline `code` example.

```javascript
// Code block example
function greet() {
  return "Hello, world!";
}
```

```txt
Code block example without any code. Note that the first example adds "javascript" besides ```, while this one adds "txt" to maintain the padding.
```
```
This is another code block example like last time, but without "txt". Could be used for one-liners, or when referencing a line from another code.
```

```txt
Example with a long word:
Lopadotemachoselachogaleokranioleipsanodrimhypotrimmatosilphioparaomelitokatakechymenokichlepikossyphophattoperisteralektryonoptekephalliokigklopeleiolagoiosiraiobaphetraganopterygon
```

Another example with a long word: Lopadotemachoselachogaleokranioleipsanodrimhypotrimmatosilphioparaomelitokatakechymenokichlepikossyphophattoperisteralektryonoptekephalliokigklopeleiolagoiosiraiobaphetraganopterygon with a case of seemingly incomprehensibilities.

## Links

[Visit homepage](/)

## Images

![Example Image](https://fakeimg.pl/1024x1024?text=Image&font=noto)

### Tables

| Header 1 | Header 2 |
| -------- | -------- |
| Data 1   | Data 2   |
