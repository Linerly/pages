---
title: Projects
date: 2023-06-14T23:11:26+07:00
layout: staticpage
---

[{{< image-left "EcoCalc (2024)" "/media/ecocalc.svg" >}}](https://ecocalc.linerly.xyz)

Calculate your potential earnings from recyclable waste!

{{< buttons/codeberg "https://codeberg.org/Linerly/ecocalc" >}}

‎
‎
‎
‎
‎
‎

[{{< image-left "EcoTrek (2023)" "/media/ecotrek.svg" >}}](https://ecotrek.linerly.xyz)

Answer a few questions to estimate your carbon footprint and learn how to reduce it in the long run!

{{< buttons/codeberg "https://codeberg.org/Linerly/ecotrek" >}}

‎
‎
‎
‎
‎
‎

[{{< image-left "Pegiatan (2023)" "/media/pegiatan.svg" >}}](https://pegiatan.linerly.xyz)

Turn your tasks into quests in Pegiatan! Once you've done your tasks, your level will automatically increase.

Unfortunately, this project isn't as polished as how I planned it to be due to the short amount of time for the school project. Until then, when I have the time, I will definitely make it better.

{{< buttons/codeberg "https://codeberg.org/Linerly/pegiatan" >}}

‎
‎
‎
‎
‎
‎

[{{< image-left "Geora (2023)" "/media/geora.svg" >}}](https://geora.linerly.xyz)

Geora is an interactive 3D map of the world using [OpenStreetMap](https://www.openstreetmap.org/about) data. Search various places and see how it looks like!

Missing something on the map? [Contribute to OpenStreetMap!](https://www.openstreetmap.org/fixthemap)

{{< buttons/codeberg "https://codeberg.org/Linerly/geora" >}}

‎
‎
‎
‎
‎
‎

[{{< image-left "Apkes (2022)" "/media/apkes.svg" >}}](https://apkes.linerly.xyz)

Apkes is a simple health app that gives you tips on living a healthy life, along with inspirational quotes to live it up!

You can also write about how your day went, and it will be stored locally until you delete it.

{{< buttons/codeberg "https://codeberg.org/Linerly/apkes" >}}

‎
‎
‎
‎
‎
‎

# The [MediaPlay Website](https://mediaplay-discord.netlify.app)

**2020**

This one is for a community that I've been in for years---MediaPlay!

Just like this website, it has been a long way since... this:

![The before and after versions of the website.](/media/mediaplay_website_history.webp)

In fact, it uses most of the code that I've used in building my website. Nowadays, it's a place to see how far I can use Hugo to make customized websites for many purposes.

‎
‎
‎
‎
‎
‎

# LinerlyBot ([Discord](https://codeberg.org/Linerly/LinerlyBot-Discord) & [Matrix](https://codeberg.org/Linerly/LinerlyBot-Matrix))

**2019**

These are my attempts at making a bot on both [Discord](https://discord.com) and [Matrix](https://matrix.org).

Initially, it was only available on Discord. As I moved away from using Discord though, I also ported the bot to make it work on Matrix.

It does have a few fun commands and a shop functionality, but that's about it. Over the years, things have changed and so does the libraries and the APIs---eventually, the Discord bot breaks.

On the other hand, the Matrix bot isn't functioning anymore. I just don't have time to maintain the bots (both the Discord and the Matrix one)!

‎
‎
‎

# [linerly.xyz](/en)

**2019**

My website, obviously. I made this website to learn web development, that's why the projects in here are web apps! It wasn't long before [I migrated it to Codeberg](https://codeberg.org/Linerly/pages). Before then, it was on GitHub.

It started as multiple HTML pages, but I had to edit all of them manually if I want to update, say, the navigation bar. Sometimes even re-writing them.

Up until August 2022, I finally took the opportunity to convert it into a Hugo site which you are looking at it right now! This allows me to not just have a new blog (with the pre-existing contents of the old blog) that's consistent with the website's theme, but also being able to make pages in Markdown rather than HTML (though it can be limited sometimes).
