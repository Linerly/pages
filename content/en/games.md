---
title: Games
layout: staticpage
---

These are games (and interactive things) that I've made using [Scratch](https://scratch.mit.edu); [TurboWarp Packager](https://packager.turbowarp.org) is used to display them here.

---

# [Protect the Balloon 2](https://scratch.mit.edu/projects/828152923) (2021)

*Don't let any falling shapes pop your balloon! How long can it survive?*

{{< scratch/project "protect-the-balloon-2" >}}

{{< scratch/about "Move the mouse to the left or right to dodge the objects." "Swipe to the left or right to dodge the objects." `This is a new version of the old, buggy <a href="https://scratch.mit.edu/projects/237677192" target="_blank">Protect the Balloon</a>. This was made for my previous school festival.` `Music used in-game:<ul><li>“Midnight Tale” by Kevin MacLeod (incompetech.com)</li><li>“Derp Nugget” by Kevin MacLeod (incompetech.com)</li><li>“Onion Capers” by Kevin MacLeod (incompetech.com)<br />Licensed under <a href="https://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons: By Attribution 4.0 License</a></li></ul>` >}}

# [Nitrous](https://scratch.mit.edu/projects/828152923) (2019)

*A playful operating system.*

{{< scratch/project "nitrous" >}}

{{< scratch/about "For the Breakout game, use the left and right arrows to move the platform." `<i>No mobile controls for the Breakout game; use an external keyboard.</i>` `Nitrous (formerly Nitro OS) was a little experiment to create an operating system in Scratch. It is the “successor” of <a href="https://scratch.mit.edu/projects/229140749" target="_blank">LineOS</a>, a remixed project from <a href="https://scratch.mit.edu/projects/199121210" target="_blank">Scratch Phone</a>. The current version you're seeing is the 2022 revamp where the user interface has changed from a stiff blue look into a more friendly, colorful (though still blue) look. The revamp was made for my previous school festival.` `All assets were made using <a href="https://inkscape.org" target="blank">Inkscape</a>; startup sound was made using <a href="https://lmms.io" target="_blank">LMMS</a>; some icons were from <a href="https://www.flaticon.com" target="_blank">Flaticon</a> in the older version; as for the font, it uses <a href="https://rsms.me/inter" target="_blank">Inter</a>.` >}}

# [Escape the House!](https://scratch.mit.edu/projects/320573082) (2018)

*The boy has been locked in the house, and the boy can't escape somehow. Help the boy escape from the house!*

{{< scratch/project "escape-the-house" >}}

{{< scratch/about "Follow the dialogue and click on things as instructed." "Follow the dialogue and tap on things as instructed." "This is my most popular Scratch game — it reached over 52.000 views!" `<ul><li>Most assets from <a href="https://www.flaticon.com" target="_blank">Flaticon</a>, character made by myself</li><li>Music used in-game: “Funny Scene” by Creative Station</li></ul>` >}}

# [Space Platformer](https://scratch.mit.edu/projects/269552167) (2018)

{{< scratch/project "space-platformer" >}}

{{< scratch/about "Use arrow keys or WASD to move Scratch Cat." `<i>No mobile controls; use an external keyboard.</i>` "The third platformer I've made in Scratch, but space-themed." `<ul><li>Most assets from the Scratch library with few modifications</li><li>Music used in-game: “Space Travel on an Eight Oh Eight” by TeknoAXE</li></ul>` >}}

# [\*_\* Platformer 2!](https://scratch.mit.edu/projects/235570018) (2018)

{{< scratch/project "platformer-2" >}}

{{< scratch/about "Use arrow keys to move the character." `<i>No mobile controls; use an external keyboard.</i>` "The second platformer I've made in Scratch." `<ul><li>Most assets made using the Scratch image editor</li><li>Music used in-game: “Ukulele” and “Going Higher” by Bensound.com</li></ul>` >}}

# [\*_\* Platformer!](https://scratch.mit.edu/projects/235240074) (2018)

{{< scratch/project "platformer" >}}

{{< scratch/about "Use the left and right arrow keys to move and use the spacebar to jump." `<i>No mobile controls; use an external keyboard.</i>` `The first platformer I've made in Scratch; <a href="https://scratch.mit.edu/users/ceebee" target="_blank">@ceebee</a> (part of the Scratch Team) even gave this game a ❤️!` `<ul><li>Most assets made using the Scratch image editor</li><li>Music used in-game: “Abstract Reality” by Vexento</li></ul>` >}}

*There are more in [my Scratch profile](https://scratch.mit.edu/users/DancingLine/projects/) and [my Roblox profile](https://www.roblox.com/users/500327794/profile#!/creations), too.*