---
title: "Creations"
date: 2022-08-21T23:27:46+07:00
layout: staticpage
---

Here are some *(if not most)* of my creations!

> Click on the image to view the original version. Warning: Some of them are **large!**

{{< gallery >}}

{{< figure src="creations/bedroom-new.png" tool="Blender" alt="A cozy bedroom render featuring a comfortable bed and a bright window letting in natural light.">}}
{{< figure src="creations/blob-with-teapot.png" tool="Blender" alt="A render of a blob character beside a teapot with the words 'blob with teapot' above.">}}
{{< figure src="creations/blue-layers-blur-dark.png" tool="Inkscape" alt="A minimal vector blue terrain in daytime.">}}
{{< figure src="creations/blue-layers-blur-light.png" tool="Inkscape" alt="A minimal vector blue terrain in nighttime.">}}
{{< figure src="creations/channel-art.png" tool="Blender" alt="My YouTube channel art with various 3D shapes and fabrics with the main text 'Linerly' resting on the floor with the text 'Subscribe for Random Videos!' in front of it.">}}
{{< figure src="creations/christmas.png" tool="Blender" alt="My Cube Humanoid character waving while wearing Santa's hat with falling snow in the background.">}}
{{< figure src="creations/cube-on-fire.png" tool="Blender" alt="A cube set on fire.">}}
{{< figure src="creations/cube-on-ocean.png" tool="Blender" alt="A cube floating on the ocean.">}}
{{< figure src="creations/cubie-but-rtx-is-on.png" tool="Blender" alt="My latest Cubie character tilting to the right with a striped background, rendered in 3D with ray tracing.">}}
{{< figure src="creations/cubieinspected.png" tool="Inkscape" alt="My older Cubie character in a dark night setting with glowing outlines.">}}
{{< figure src="creations/cubie-in-the-fall.png" tool="Blender" alt="My Cube Humanoid character waving with falling leaves in the background.">}}
{{< figure src="creations/cubie.png" tool="Inkscape" alt="First Cubie character. It has lots of stripes in the background. The face looks a bit sparse.">}}
{{< figure src="creations/easter-eggs.png" tool="Krita" alt="Easter eggs on the grass terrain.">}}
{{< figure src="creations/environment.png" tool="Blender" alt="Lots of pine trees on the grass terrain in daytime.">}}
{{< figure src="creations/flower.png" tool="Krita" alt="A white flower with a blurry terrain background.">}}
{{< figure src="creations/grid.png" tool="Inkscape" alt="A blue grid with my Cube Humanoid character along with some other shapes with its wireframe visible. The background has subtle random text characters.">}}
{{< figure src="creations/grid-3d.png" tool="Blender" alt="A 3D blue grid terrain with a bloom effect on the camera.">}}
{{< figure src="creations/latest-linerly-cubie.png" tool="Inkscape" alt="A refined version of my Cubie character. The stripes in the background are wider.">}}
{{< figure src="creations/linerly-cubie-rtx.png" tool="Blender" alt="My Cubie character in 3D, ray-traced.">}}
{{< figure src="creations/mountains.png" tool="Inkscape" alt="A vector mountain terrain with stars and the moon in the background, along with slight clouds.">}}
{{< figure src="creations/mpd-anniversary.png" tool="Krita" alt="Three members of MediaPlay Discord (JoshuaTNTGamer, TechNewVideo, and RockyR) in front of a tall building with the MPD logo above with fireworks in the background.">}}
{{< figure src="creations/new-year.png" tool="Blender" alt="My Cube Humanoid character with arms up while wearing a hat with the words 'Happy New Year!' around it on a black background with confetti falling down.">}}
{{< figure src="creations/oc-motion-blur.png" tool="Blender" alt="My Cube Humanoid character being slightly muted on a gray background, waving its arms with a motion blur effect.">}}
{{< figure src="creations/pc-background.jpg" tool="GIMP" alt="A 3D render PC wallpaper with blurry neon shapes in the background with fog around. There is a button in the center with a gradient outline.">}}
{{< figure src="creations/pc-wallpaper.png" tool="Blender" alt="A 3D render PC wallpaper with rough hills and a mountain made from a subdivided plane, being illuminated with colorful lights, in nighttime with blurry stars in the sky.">}}
{{< figure src="creations/phone-wallpaper-1.png" tool="Blender" alt="A phone wallpaper with a smooth hilly terrain made from a subdivided plane, illuminated with colorful lights. The sky is approaching nighttime with clear stars.">}}
{{< figure src="creations/phone-wallpaper-2.jpg" tool="Blender" alt="Raindrops staying still with an HDRI background of a grassy terrain with houses far away.">}}
{{< figure src="creations/phone-wallpaper.png" tool="GIMP" alt="A vector phone wallpaper with a Cubie character going upwards with clouds around. The sun is above the character.">}}
{{< figure src="creations/plain-donut-render.jpg" tool="Blender" alt="A 3D render of a donut with no materials on the floor being illuminated from the left side of the donut.">}}
{{< figure src="creations/profile-picture.png" tool="Blender" alt="My current profile picture of a 3D render with my Cube Humanoid character waving.">}}
{{< figure src="creations/rock.png" tool="Blender" alt="A 3D render of rocks on a rocky terrain in clear daytime. There is also a glassy rock in front.">}}
{{< figure src="creations/scenery.png" tool="Krita" alt="A drawing of grassy terrain in daytime with clouds and fog in the background.">}}
{{< figure src="creations/sky.png" tool="Krita" alt="A watercolor painting of the sky with realistic clouds and a blurry sun in the background.">}}
{{< figure src="creations/winter-stargazing-retreat.png" tool="Krita" alt="A snow couple holding hands with twig arms looking at the night sky with lots of stars. They're standing on a snowy grass terrain with pine trees and mountains in the background.">}}

{{</ gallery >}}
