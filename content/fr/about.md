---
title: À propos
date: 2024-10-17T20:50:00+07:00
layout: staticpage
---

# Bonjour ! 👋

Je suis développeur web, designer et artiste. Je fais d'autres choses pendant mon temps libre, mais ce n'est pas vraiment ma principale activité.