---
title: "Mengapa kamu seharusnya menerapkan minat dan bakat dalam proyek sekolah"
date: 2023-02-10T08:28:35+07:00
---

Proyek sekolah bisa menjadi hal yang membosankan, tetapi kamu bisa membuatnya jauh lebih menyenangkan — pamerkan minat dan bakatmu!

Selama proyek ini, kamu tidak hanya akan membuat prosesnya lebih menyenangkan, tetapi juga bisa memukau gurumu dan menonjol dari teman sekelasmu.

# Membuat proyek lebih menyenangkan

Ketika bekerja pada proyek yang digemari dan memiliki kemampuan, kamu akan lebih termotivasi untuk memberikan usaha tambahan untuk membuatnya luar biasa.
Membawa sudut pandang yang unik

Setiap siswa memiliki kelebihan mereka sendiri, dan dengan menggunakan kelebihanmu dalam proyek, kamu bisa menambahkan sudut pandang yang segar dan unik yang tidak bisa dilakukan oleh siapa pun.

# Belajar kemampuan baru!

Proyek sekolah adalah kesempatan yang baik untuk belajar hal-hal baru dan menantang diri sendiri. Misalnya, aku pernah membuat [beberapa](https://apkes.linerly.tk) [aplikasi web](https://geora.linerly.tk) untuk proyek sekolah, yang saya buat (sedikit) lebih maju dari proyek ke proyek.

# Membuatmu menonjol dari kerumunan

Dengan menggabungkan minat dan bakat ke dalam proyek, kamu akan menunjukkan kepada guru bahwa kamu tidak hanya asal-asalan dan serius tentang pendidikanmu.

Pada saat berikutnya kamu bekerja pada proyek sekolah, pikirkan tentang kemampuan dan minat apa yang bisa dibawa dan bagaimana bisa membuat dampak yang abadi. Jangan hanya puas dengan nilai yang cukup, tunjukkan potensimu!
