---
title: "Akhirnya punya domain sendiri!"
date: 2023-08-01T20:34:06+07:00
---

Sekarang, situs webku berada di linerly.xyz (sebelumnya di linerly.codeberg.page, linerly.tk, dan linerly.github.io). Enggak seperti domain linerly.tk yang lama (dikasih oleh teman daring), domain linerly.xyz ini dimiliki sendiri. Tentu saja, aku membayarnya sendiri dengan harga yang lumayan murah (Rp28.000,-).

Sayangnya dengan domain yang gratis, enggak bisa dijamin kalau bisa dipakai untuk jangka waktu yang lama. Untungnya domain yang lama belum dipakai untuk hal-hal yang bersifat permanen (seperti surel). Dengan domain baru ini, aku bisa memakainya untuk banyak hal sekarang!

Memang agak repot ganti-ganti pengaturan Pages sampai dengan profil yang ada di berbagai situs web lainnya. Tetapi akhirnya ini _worth it_---enggak akan berubah untuk waktu yang lama.
