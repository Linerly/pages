---
title: "Hari (hampir) tanpa gawai"
date: 2023-09-09T20:15:33+07:00
---

Berapa lama kamu biasanya daring? Mungkin terlalu lama. Apakah kamu selalu mencoba untuk selalu terhubung dengan banyak hal di ponsel atau perangkat lainnya? Sebagian besar dari kita melakukan hal itu.

Aku enggak sadar, tetapi sepertinya aku bisa merasa lelah (mungkin secara mental?) karena terlalu sering pakai teknologi. Ini sampai saat kelasku memutuskan untuk mengadakan 'tantangan' _One Day No HP_, yang dalam bahasa Indonesia artinya Satu Hari Tanpa Ponsel. Meskipun sebenarnya melarang penggunaan HP selama sehari, aku juga memutuskan untuk tidak memakai laptopku. Seharusnya enggak kelewatan hal-hal yang penting.

Kebetulan, aku juga melakukan survei ke berbagai tempat, jadi itu mengisi waktu yang aku gunakan tanpa terkoneksi. Sisanya adalah tidur sebelum aku melakukan survei (dan tentunya aktivitas lain yang juga dilakukan sehari-hari, ya).

Tantangan ini seharusnya berlaku juga untuk orang tuaku, tetapi sepertinya mereka perlu menggunakan ponselnya untuk urusan penting. Masalahnya harus memakai ponselku sebentar untuk mengatur ulang NextDNS untuk semua perangkat di keluarga dan mematikan kontrol orang tua (jadi aku saja yang bisa kontrol DNS-nya).

Setelah tantangan berakhir, aku juga memanfaatkan waktu untuk menjadikan ponselku lebih minimalis — menghapus aplikasi yang tidak terpakai dan hanya menyimpan aplikasi yang paling sering aku gunakan di layar utama dalam satu layar.

Aku juga mencoba menggunakan mode gelap otomatis, jadi siang hari menggunakan tema terang, dan malam hari menggunakan tema gelap. Tetapi ternyata itu cepat menjadi mengganggu mata, jadi aku tetap menggunakan mode gelap sepanjang hari. Toh perangkatku juga berwarna hitam.

Di ponsel, aku juga menggunakan mode skala abu-abu saat waktu tidur. Ini bisa membantu mengurangi gangguan, tetapi tidak begitu berguna jika perlu melihat konten yang mengandalkan warna.
