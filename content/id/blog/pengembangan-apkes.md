---
title: "Pengembangan Apkes"
date: 2022-08-24T08:33:09+07:00
---

[Postingan ini juga tersedia dalam bahasa Inggris (aslinya).](/en/blog/the-development-of-apkes)

# Pernah menggunakan aplikasi web?

Anda mungkin sudah pernah menggunakan aplikasi web di ponsel atau di laptop tanpa mengetahuinya, tetapi sebenarnya aplikasi web adalah situs web yang dirancang seperti aplikasi.

# Apa itu Apkes?

Apkes adalah gabungan “aplikasi” dan “kesehatan”. Aplikasinya dibuat dengan [kerangka kerja Svelte](https://svelte.dev) dan lainnya seperti [Tailwind](https://tailwindcss.com) untuk membuat pengembangan cepat dalam kurang lebih dua minggu.

![Tangkapan layar Apkes](/media/Screenshot_20220824_083745.png "Sebuah tangkapan layar Apkes. Terlihat sederhana, bukan?")

Awalnya bahasa Indonesia hanya ada di Apkes, tetapi dengan _internationalization_ (alias terjemahan aplikasi) dapat mendukung bahasa yang lain seperti bahasa Inggris. Bahasa yang lain dapat ditambahkan melalui tarik permintaan di [repositori Codeberg](https://codeberg.org/Linerly/apkes/pulls).

Inspirasi untuk aplikasi webnya sebenarnya datang dari aplikasi meditasi.

Saya memang membuatnya sederhana sebaik mungkin supaya bisa dipeliharai ketika fitur baru ditambahkan.

Seperti proyek saya yang lain seperti situs web ini, Apkes itu sumber terbuka — siapa pun dapat melihat kode sumber untuk melihat bagaimana bekerjanya, [lihat kode sumbernya di Codeberg](https://codeberg.org/Linerly/apkes).

# Cara pembuatan

Ada banyak hal yang digunakan di belakang Apkes — semuanya sumber terbuka — seperti

- Pustaka dan kerangka kerja: [Svelte](https://svelte.dev), [Tailwind](https://tailwindcss.com), [Vite](https://vitejs.dev), untuk kerangka kerja, tema aplikasi, dan pembangunan aplikasi sendiri-sendiri.
- Platform untuk layanan: [Codeberg](https://codeberg.org), [Codeberg Pages](https://codeberg.page), [Codeberg CI](https://codeberg.org/Codeberg-CI/request-access#getting-access-to-the-codeberg-ci), [deSEC](https://desec.io), untuk layanan kode sumber, layanan aplikasi web, pembangunan aplikasi, dan DNS sendiri-sendiri.
- Alat-alat: [Git](https://git-scm.com) dan [VS Codium](https://vscodium.com), untuk pengendalian versi dan editor kode sendiri-sendiri

Selanjutnya, saya juga mencoba aplikasinya dengan laptop saya dan ponsel saya dengan Chromium dan Firefox. Saya belum pernah mencoba pada perangkat Apples — atau peramban web berbasis WebKit — tetapi seharusnya bisa.

# Bagaimana _sebenarnya_ cara aplikasinya bekerja

Fitur-fitur dipisahkan sebagai komponen Svelte supaya tidak berantakan.

Tidak seperti kerangka kerja lainnya, Svelte mengompilasikan aplikasinya supaya dapat dijalankan dengan cepat di peramban web.

Saat ini, terjemahannya tidak di dalam aplikasi, tetapi diunduh secara langsung berdasarkan pengaturan bahasa di peramban web.

Setelah semua itu, aplikasinya dapat digunakan tanpa koneksi internet — sampai Anda memuat ulang lamannya — dan semua data disimpan secara lokal dalam peramban web.

# Fitur dalam Apkes

Saat ini, hanya ada 3 fitur (alias “widget”) di Apkes — Saran, Kutipan, dan Refleksi.

- **Saran** — saran tentang hidup sehat.
- **Kutipan** — baca kutipan tentang _mengapa_ Anda harus hidup sehat.
- **Refleksi** — ketik apa pun yang Anda punya di pikiran, seperti perasaan atau hal-hal yang telah terjadi. Ini juga disimpan secara otomatis, supaya Anda dapat melihat kembali refleksi Anda selama Anda tidak menghapus data peramban web.

# Kendala selama pengembangan

Sperti dengan pengembangan perangkat lunak apa pun, kendala dapat terjadi kapan saja. Ini hal-hal untuk diingat: _sabar, tenang, dan coba mencari solusinya_. Masih belum bisa menemukan solusinya? Tinggalkan selama beberapa waktu, lalu kembali lagi ketika Anda _siap_.

# Hasilnya…

Anda dapat melihatnya sendiri — coba aplikasinya di https://apkes.linerly.tk

Masukan, ide, dan hal lainnya diapresiasikan dalam [repositori Codeberg](https://codeberg.org/Linerly/apkes/issues) (dalam [Masalah](https://codeberg.org/Linerly/apkes/issues)) atau [mengirimkannya secara langsung ke saya](/id/media-sosial). :)
