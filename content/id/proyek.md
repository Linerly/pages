---
title: Proyek
date: 2023-06-14T23:18:45+07:00
layout: staticpage
---

[{{< image-left "EcoCalc (2024)" "/media/ecocalc.svg" >}}](https://ecocalc.linerly.xyz)

Hitung potensi penghasilanmu dari sampah yang dapat didaur ulang!

{{< buttons/codeberg-id "https://codeberg.org/Linerly/ecocalc" >}}

‎
‎
‎

[{{< image-left "EcoTrek (2023)" "/media/ecotrek.svg" >}}](https://ecotrek.linerly.xyz)

Jawab beberapa pertanyaan untuk memperkirakan jejak karbonmu dan pelajari cara menguranginya demi kebaikan bersama!

{{< buttons/codeberg-id "https://codeberg.org/Linerly/ecotrek" >}}

‎
‎
‎

[{{< image-left "Pegiatan (2023)" "/media/pegiatan.svg" >}}](https://pegiatan.linerly.xyz)

Jadikan tugasmu sebagai misi di Pegiatan! Ketika kamu sudah melakukan beberapa tugas, levelmu akan naik secara otomatis.

Sayangnya, proyek ini tidak seperti yang aku harapkan karena waktu yang ditentukan untuk proyek sekolah. Kalau ada waktu, aku pasti akan membuat ini lebih baik. Ini sih masih lumayan---yang penting bisa.

{{< buttons/codeberg-id "https://codeberg.org/Linerly/pegiatan" >}}

‎
‎
‎

[{{< image-left "Geora (2023)" "/media/geora.svg" >}}](https://geora.linerly.xyz)

Geora adalah peta dunia 3D yang interaktif menggunakan data [OpenStreetMap](https://www.openstreetmap.org/about). Cari berbagai tempat dan lihat seperti apa di peta!

Ada yang kurang di peta? [Mulai berkontribusi di OpenStreetMap!](https://www.openstreetmap.org/fixthemap)

{{< buttons/codeberg-id "https://codeberg.org/Linerly/geora" >}}

‎
‎
‎

[{{< image-left "Apkes (2022)" "/media/apkes.svg" >}}](https://apkes.linerly.xyz)

Apkes adalah aplikasi kesehatan sederhana yang memberikan kiat tentang menjalani hidup sehat, serta kutipan inspiratif untuk menjalani hidup dengan semangat!

Kamu juga dapat menulis tentang bagaimana hari kamu berjalan, dan hal itu akan disimpan secara lokal sampai dihapus.

{{< buttons/codeberg-id "https://codeberg.org/Linerly/apkes" >}}

‎
‎
‎

# Situs Web [MediaPlay](https://mediaplay-discord.netlify.app)

**2020**

Kalau yang ini untuk komunitas daring yang sudah ada sejak beberapa tahun lalu---MediaPlay!

Seperti situs web ini, sudah ada banyak perubahan sejak... ini:

![Versi lama dan baru dari situs webnya.](/media/mediaplay_website_history.webp)

Faktanya kebanyakan kode di situs web MediaPlay dari kode situs webku sendiri. Sekarang, ini merupakan tempat bagiku untuk mencoba melihat seberapa jauh aku bisa membuat situs web yang memiliki banyak fungsi menggunakan Hugo.

‎
‎
‎

# LinerlyBot ([Discord](https://codeberg.org/Linerly/LinerlyBot-Discord) & [Matrix](https://codeberg.org/Linerly/LinerlyBot-Matrix))

**2019**

Ini adalah percobaan membuat bot [Discord](https://discord.com) dan [Matrix](https://matrix.org).

Awalnya, bot-nya hanya tersedia di Discord. Lama-lama aku semakin jarang menggunakan Discord, jadi aku mengatur kodenya supaya bekerja di Matrix juga.

Di dalam bot ada beberapa perintah-perintah yang seru dan juga ada toko, tetapi itu saja untuk sementara. Dengan berjalannya waktu, banyak hal yang sudah berubah (termasuk pustaka dan API-nya)---lama-lama, bot-nya sudah tidak bisa bekerja lagi.

Kalau yang untuk bot Matrix, sayangnya juga belum bisa juga. Masih sibuk dengan hal yang lain!

‎
‎
‎

# [linerly.xyz](/id)

**2019**

Situs webku, jelas. Aku membuat situs web ini untuk belajar pengembangan web, itulah mengapa proyek-proyek di sini adalah aplikasi web! Tak lama kemudian, [aku pindah ke Codeberg](https://codeberg.org/Linerly/pages). Sebelumnya semua itu ada di GitHub.

Awalnya terdiri dari beberapa halaman HTML, tetapi aku harus mengedit semuanya secara manual kalau aku mau memperbarui, misalnya, bilah navigasi. Kadang-kadang bahkan harus menulis ulang semuanya.

Hingga Agustus 2022, akhirnya aku mengambil kesempatan untuk mengubahnya menjadi situs Hugo yang sedang kamu lihat sekarang! Ini tidak hanya memungkinkan aku memiliki blog baru (dengan konten lama yang sudah ada) yang konsisten dengan tema situs web, tetapi juga dapat membuat halaman dalam format Markdown daripada HTML (meskipun terkadang ada batasannya).
