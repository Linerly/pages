---
title: "Kreasi"
date: 2022-08-21T23:27:46+07:00
layout: staticpage
---

Ini adalah beberapa kreasi yang aku buat!

> Klik pada gambar untuk melihat versi asli. Peringatan: Beberapa ada yang ukurannya **besar!**

{{< gallery >}}  

{{< figure src="creations/bedroom-new.png" tool="Blender" alt="Render kamar tidur yang nyaman dengan tempat tidur yang nyaman dan jendela terang yang membiarkan cahaya alami masuk.">}}  
{{< figure src="creations/blob-with-teapot.png" tool="Blender" alt="Render karakter blob di samping teko dengan tulisan 'blob with teapot' di atasnya.">}}  
{{< figure src="creations/blue-layers-blur-dark.png" tool="Inkscape" alt="Ilustrasi vektor minimalis berupa pegunungan biru di siang hari.">}}  
{{< figure src="creations/blue-layers-blur-light.png" tool="Inkscape" alt="Ilustrasi vektor minimalis berupa pegunungan biru di malam hari.">}}  
{{< figure src="creations/channel-art.png" tool="Blender" alt="Desain channel art YouTube saya dengan berbagai bentuk 3D dan kain, dengan teks utama 'Linerly' yang berada di lantai serta teks 'Subscribe for Random Videos!' di depannya.">}}  
{{< figure src="creations/christmas.png" tool="Blender" alt="Karakter humanoid kubus saya melambaikan tangan sambil memakai topi Santa dengan latar belakang salju yang turun.">}}  
{{< figure src="creations/cube-on-fire.png" tool="Blender" alt="Sebuah kubus yang terbakar.">}}  
{{< figure src="creations/cube-on-ocean.png" tool="Blender" alt="Sebuah kubus mengapung di lautan.">}}  
{{< figure src="creations/cubie-but-rtx-is-on.png" tool="Blender" alt="Karakter cubie terbaru saya miring ke kanan dengan latar belakang bergaris, dirender dalam 3D dengan ray tracing.">}}  
{{< figure src="creations/cubieinspected.png" tool="Inkscape" alt="Karakter cubie lama saya dalam suasana malam gelap dengan garis luar karakter yang bercahaya.">}}  
{{< figure src="creations/cubie-in-the-fall.png" tool="Blender" alt="Karakter humanoid kubus saya melambaikan tangan dengan latar belakang daun-daun yang berjatuhan.">}}  
{{< figure src="creations/cubie.png" tool="Inkscape" alt="Karakter cubie pertama saya. Memiliki banyak garis-garis di latar belakang. Ekspresi wajahnya terlihat agak kosong.">}}  
{{< figure src="creations/easter-eggs.png" tool="Krita" alt="Telur Paskah di atas rerumputan.">}}  
{{< figure src="creations/environment.png" tool="Blender" alt="Banyak pohon pinus di atas padang rumput pada siang hari.">}}  
{{< figure src="creations/flower.png" tool="Krita" alt="Bunga putih dengan latar belakang rerumputan yang buram.">}}  
{{< figure src="creations/grid.png" tool="Inkscape" alt="Jaring biru dengan karakter humanoid kubus saya beserta beberapa bentuk lain dengan wireframe yang terlihat. Latar belakangnya memiliki teks acak yang samar.">}}  
{{< figure src="creations/grid-3d.png" tool="Blender" alt="Jaring 3D biru dengan efek bloom pada kamera.">}}  
{{< figure src="creations/latest-linerly-cubie.png" tool="Inkscape" alt="Versi terbaru karakter cubie saya. Garis-garis di latar belakangnya lebih lebar.">}}  
{{< figure src="creations/linerly-cubie-rtx.png" tool="Blender" alt="Karakter cubie saya dalam 3D, dengan ray tracing.">}}  
{{< figure src="creations/mountains.png" tool="Inkscape" alt="Ilustrasi vektor pegunungan dengan bintang dan bulan di latar belakang, serta beberapa awan tipis.">}}  
{{< figure src="creations/mpd-anniversary.png" tool="Krita" alt="Tiga anggota MediaPlay Discord (JoshuaTNTGamer, TechNewVideo, dan RockyR) berdiri di depan gedung tinggi dengan logo MPD di atasnya, serta kembang api di latar belakang.">}}  
{{< figure src="creations/new-year.png" tool="Blender" alt="Karakter humanoid kubus saya mengangkat tangan sambil memakai topi dengan tulisan 'Happy New Year!' di sekitarnya pada latar belakang hitam dengan confetti yang berjatuhan.">}}  
{{< figure src="creations/oc-motion-blur.png" tool="Blender" alt="Karakter humanoid kubus saya tampak sedikit memudar di latar belakang abu-abu, melambaikan tangan dengan efek blur gerakan.">}}  
{{< figure src="creations/pc-background.jpg" tool="GIMP" alt="Wallpaper PC hasil render 3D dengan bentuk neon yang buram di latar belakang serta kabut di sekelilingnya. Ada sebuah tombol di tengah dengan garis luar bergradasi.">}}  
{{< figure src="creations/pc-wallpaper.png" tool="Blender" alt="Wallpaper PC hasil render 3D dengan bukit kasar dan gunung yang dibuat dari bidang tersegmentasi, diterangi lampu warna-warni, dalam suasana malam dengan bintang yang buram di langit.">}}  
{{< figure src="creations/phone-wallpaper-1.png" tool="Blender" alt="Wallpaper ponsel dengan bukit mulus yang dibuat dari bidang tersegmentasi, diterangi cahaya warna-warni. Langit hampir malam dengan bintang-bintang yang terlihat jelas.">}}  
{{< figure src="creations/phone-wallpaper-2.jpg" tool="Blender" alt="Tetesan hujan yang diam dengan latar belakang HDRI berupa padang rumput dengan rumah-rumah di kejauhan.">}}  
{{< figure src="creations/phone-wallpaper.png" tool="GIMP" alt="Wallpaper ponsel vektor dengan karakter kubus yang naik ke atas dengan awan di sekitarnya. Matahari berada di atas karakter tersebut.">}}  
{{< figure src="creations/plain-donut-render.jpg" tool="Blender" alt="Render 3D donat tanpa material di lantai yang diterangi dari sisi kiri donat.">}}  
{{< figure src="creations/profile-picture.png" tool="Blender" alt="Foto profil saya saat ini berupa render 3D dengan karakter humanoid saya melambaikan tangan.">}}  
{{< figure src="creations/rock.png" tool="Blender" alt="Render 3D bebatuan di atas tanah berbatu pada siang hari yang cerah. Ada juga batu kaca di bagian depan.">}}  
{{< figure src="creations/scenery.png" tool="Krita" alt="Lukisan padang rumput di siang hari dengan awan dan kabut di latar belakang.">}}  
{{< figure src="creations/sky.png" tool="Krita" alt="Lukisan cat air langit dengan awan realistis dan matahari yang buram di latar belakang.">}}  
{{< figure src="creations/winter-stargazing-retreat.png" tool="Krita" alt="Sepasang manusia salju berpegangan tangan dengan tangan dari ranting, menatap langit malam yang penuh bintang. Mereka berdiri di atas padang salju dengan pohon pinus dan pegunungan di latar belakang.">}}  

{{</ gallery >}}  