---
title: Gim
layout: staticpage
---

Di sini merupakan gim (dan hal interaktif) yang aku buat menggunakan [Scratch](https://scratch.mit.edu); [TurboWarp Packager](https://packager.turbowarp.org) digunakan untuk menampilkannya di sini.

---

# [Protect the Balloon 2](https://scratch.mit.edu/projects/828152923) (2021)

*Jangan biarkan benda-benda jatuh pada balon! Seberapa lama balonnya bisa bertahan?*

{{< scratch/project "protect-the-balloon-2" >}}

{{< scratch/about "Gerakkan tetikus ke kiri atau kanan untuk menghindari benda-benda." "Usap ke kiri atau kanan untuk menghindari benda-benda." `Ini merupakan versi baru dari <a href="https://scratch.mit.edu/projects/237677192" target="_blank"><i>Protect the Balloon</i></a> yang lawas dan banyak bermasalah. Gim ini dibuat untuk festival sekolah dahulu.` `Musik yang digunakan dalam gim:<ul><li>“Midnight Tale” oleh Kevin MacLeod (incompetech.com)</li><li>“Derp Nugget” oleh Kevin MacLeod (incompetech.com)</li><li>“Onion Capers” oleh Kevin MacLeod (incompetech.com)<br />Berlisensi di bawah <a href="https://creativecommons.org/licenses/by/4.0/" target="_blank">Creative Commons: By Attribution 4.0 License</a></li></ul>` >}}

# [Nitrous](https://scratch.mit.edu/projects/828152923) (2019)

*Sebuah sistem operasi yang menyenangkan.*

{{< scratch/project "nitrous" >}}

{{< scratch/about "Untuk gim Breakout, gunakan panah kiri dan kanan untuk menggerakkan platform." `<i>Tidak ada kontrol untuk perangkat seluler dalam gim Breakout; gunakan papan tik eksternal.</i>` `Nitrous (sebelumnya Nitro OS) adalah sebuah eksperimen kecil untuk membuat sistem operasi di Scratch. Ini adalah “penerus” dari <a href="https://scratch.mit.edu/projects/229140749" target="_blank">LineOS</a>, sebuah proyek remix dari <a href="https://scratch.mit.edu/projects/199121210" target="_blank">Scratch Phone</a>. Versi yang sedang kamu lihat adalah perombakan tahun 2022 di mana antarmuka pengguna telah diubah dari tampilan biru kaku menjadi tampilan yang lebih ramah dan berwarna (meskipun masih biru). Perombakan ini dibuat untuk festival sekolahku sebelumnya.` `Semua aset dibuat menggunakan <a href="https://inkscape.org" target="blank">Inkscape</a>; suara startup dibuat menggunakan <a href="https://lmms.io" target="_blank">LMMS</a>; beberapa ikon berasal dari <a href="https://www.flaticon.com" target="_blank">Flaticon</a> dalam versi yang lebih lama; untuk fon, menggunakan <a href="https://rsms.me/inter" target="_blank">Inter</a>.` >}}

# [Escape the House!](https://scratch.mit.edu/projects/320573082) (2018)

*Seorang anak terkunci di dalam rumah; entah bagaimana dia tidak bisa keluar. Bantu anak itu melarikan diri dari rumah!*

{{< scratch/project "escape-the-house" >}}

{{< scratch/about "Ikuti dialog dan klik pada benda-benda sesuai instruksi." "Ikuti dialog dan ketuk pada benda-benda sesuai instruksi." "Ini adalah gim Scratch aku yang paling populer — sudah mencapai lebih dari 52.000 penayangan!" `<ul><li>Kebanyakan aset dari <a href="https://www.flaticon.com" target="_blank">Flaticon</a>, karakter dibuat olehku sendiri</li><li>Musik yang digunakan dalam gim: “Funny Scene” oleh Creative Station</li></ul>` >}}

# [Space Platformer](https://scratch.mit.edu/projects/269552167) (2018)

{{< scratch/project "space-platformer" >}}

{{< scratch/about "Gunakan tombol panah atau WASD untuk menggerakkan Scratch Cat." `<i>Tidak ada kontrol untuk perangkat seluler; gunakan papan tik eksternal.</i>` "Platformer ketiga yang kubuat di Scratch, tetapi dengan tema luar angkasa." `<ul><li>Kebanyakan aset dari perpustakaan Scratch dengan sedikit modifikasi</li><li>Musik yang digunakan dalam gim: “Space Travel on an Eight Oh Eight” oleh TeknoAXE</li></ul>` >}}

# [\*_\* Platformer 2!](https://scratch.mit.edu/projects/235570018) (2018)

{{< scratch/project "platformer-2" >}}

{{< scratch/about "Gunakan tombol panah untuk menggerakkan karakter." `<i>Tidak ada kontrol untuk perangkat seluler; gunakan papan tik eksternal.</i>` "Platformer kedua yang kubuat di Scratch." `<ul><li>Kebanyakan aset dibuat menggunakan editor gambar Scratch</li><li>Musik yang digunakan dalam gim: “Ukulele” dan “Going Higher” oleh Bensound.com</li></ul>` >}}

# [\*_\* Platformer!](https://scratch.mit.edu/projects/235240074) (2018)

{{< scratch/project "platformer" >}}

{{< scratch/about "Gunakan tombol panah kiri dan kanan untuk bergerak dan gunakan spasi untuk melompat." `<i>Tidak ada kontrol untuk perangkat seluler; gunakan papan tik eksternal.</i>` `Platformer pertama yang kubuat di Scratch; <a href="https://scratch.mit.edu/users/ceebee" target="_blank">@ceebee</a> (bagian dari Tim Scratch) bahkan memberikan ❤️ pada gim ini!` `<ul><li>Kebanyakan aset dibuat menggunakan editor gambar Scratch</li><li>Musik yang digunakan dalam gim: “Abstract Reality” oleh Vexento</li></ul>` >}}

*Ada lebih banyak lagi di [profil Scratch](https://scratch.mit.edu/users/DancingLine/projects/) dan [profil Roblox](https://www.roblox.com/users/500327794/profile#!/creations) pribadi juga.*
