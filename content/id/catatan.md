---
title: Catatan
date: 2022-08-20T16:22:14+07:00
layout: staticpage
---

Situs web ini sekarang menggunakan [Hugo](https://gohugo.io) untuk membuat pengeditan mudah, terutama pada postingan blog. :)

[Kode sumber juga telah dipindahkan ke Codeberg.](https://codeberg.org/Linerly/pages)

Situs web yang baru ini juga dilayani oleh [Codeberg Pages](https://codeberg.page), dan [Codeberg CI](https://codeberg.org/Codeberg-CI/request-access#getting-access-to-the-codeberg-ci) (tetapi CI masih dalam pengujian beta tertutup).

Situs ini juga memiliki Google Analytics pada waktu yang lalu, tetapi sekarang itu telah dihilangkan dari situs ini karena kekhawatiran privasi — tetapi aku mendapatkan beberapa statistik pada situsku.

Oleh karena itu, [GoatCounter](https://www.goatcounter.com) sekarang digunakan untuk analitik — [lihat laman analitik publik pada situsku](https://stats.linerly.xyz)!
