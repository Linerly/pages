---
title: "Tentang aku"
date: 2022-08-21T19:58:15+07:00
layout: staticpage
---

**Halo!** 👋

Aku suka mengembangkan hal-hal berkaitan dengan web, desain, dan menggambar. Tentu melakukan hal-hal lain juga di waktu luang, tetapi itu bukan hal yang utama.

# Keterampilan
## Pengembangan Web
- HTML, CSS, JavaScript[^1]
- Svelte (dan SvelteKit)
- Hugo

## Pengembangan Perangkat Lunak
- Python[^2]

## Pengembangan Gim
- Godot Engine
- Scratch

## Desain
- Desain UI/UX menggunakan Inkscape dan Penpot[^3]
- Kadang menggambar dengan Krita

## Bahasa
- Indonesia (bahasa ibu)
- Inggris (C1)
- Prancis (A1)[^4]

# Sertifikasi
## Bahasa
- {{< abbr "UKBI" "Uji Kemahiran Berbahasa Indonesia" >}} — {{< abbr "696" "Sangat Unggul" >}} (2024-11-20)
- {{< abbr "TOEFL ITP" "Test of English as a Foreign Language - Institutional Testing Program" >}} — {{< abbr "627" "C1" >}} (2025-02-11)

## Jaringan
- {{< abbr "MTCNA" "MikroTik Certified Network Associate" >}} (2025-02-20)[^5]

# Kelebihan[^6]
- Setiap kali ada masalah, biasanya bisa diselesaikan… Tetapi terkadang bisa memakan waktu. Terkadang harus cari ide lain ketika sesuatu tidak berhasil berkali-kali.
- Suka membuat proyek dari awal, baik itu aplikasi web, permainan, atau bahkan bot sederhana.
- Belajar setiap hari merupakan suatu keharusan. Itulah caranya aku bisa sampai sini.
- Sudah terbiasa bekerja pada proyek jangka panjang, bahkan ketika proyek tersebut menjadi tantangan, seperti memelihara server rumahku atau melakukan pengawakutuan pada aplikasi… semoga saja sih bisa.

# Motivasi
- Buat hal yang berguna, atau setidaknya yang seru.
- Proyek bukanlah sebuah tujuan; proyek merupakan perjalanan!
- Menjadi terbuka berarti bersama-sama menjadi lebih baik. Oleh karena itu, kebanyakan proyekku bersumber terbuka.

[^1]: Biasanya pakai TypeScript kalau membuat aplikasi web Svelte, tapi belum tahu cara pakainya yang benar… cara pakai *type*-nya sendiri yang belum benar (namanya juga TypeScript).
[^2]: Untuk sementara…
[^3]: WIP: Bagikan konsep UI.
[^4]: Masih belajar!
[^5]: Di ujian dapat 100%. Ya, memang bersifat *open-book*, sih.
[^6]: Kekuatan? *Strength?*