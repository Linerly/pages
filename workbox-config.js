module.exports = {
	globDirectory: 'public/',
	globPatterns: [
		'**/*.{html,png,jpg,json,webp,woff,woff2}'
	],
	swDest: 'public/sw.js',
	ignoreURLParametersMatching: [
		/^utm_/,
		/^fbclid$/
	]
};